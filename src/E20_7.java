import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class RectangleFrame extends JFrame implements ActionListener{

    JMenuBar menuBar = new JMenuBar();
    JMenu menu = new JMenu("Rectangle generator");

    JMenuItem more = new JMenuItem("More!");
    JMenuItem less = new JMenuItem("Less!");
    private List<Rectangle> rectangles = new ArrayList<>();


    private class Rectangle{
        public int x;
        public int y;
        public Color c;
    }

    public RectangleFrame(){
        super("Random Rectangles");
        setSize(600,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        menu.add(more);
        menu.add(less);

        menuBar.add(menu);

        setJMenuBar(menuBar);

        //when menu buttons are clicked
        more.addActionListener(this);
        less.addActionListener(this);

        setVisible(true);

        //Just so the frame starts with one rectangle instead of none
        Rectangle rect = new Rectangle();
        Random randomNumber = new Random();
        rect.x = randomNumber.nextInt(200-10)+10;
        rect.y = randomNumber.nextInt(200-20)+20;
        rect.c = new Color(200, 50, 50);

        this.rectangles.add(rect);
    }

    public void paint (Graphics g){
        super.paint(g);
        for (Rectangle r: rectangles){
            paint(g,r);
        }
    }

    private void paint(Graphics g, Rectangle r) {
        g.setColor(r.c);
        g.fillRect(r.x,r.y,50,50);
        g.setColor(Color.BLACK);
        g.drawRect(r.x,r.y ,50,50);
    }

    //counts how many rectangles are inside list and double the ammount
    public void addRectangle(){
        int numberOfRect = rectangles.size();
        Random randomNumber = new Random();

/*        if(numberOfRect == 0){
            Rectangle rect = new Rectangle();
            // I tried making the rectangles appear completly inside the frame but some will still have some of their area outside of frame. Still visible tho.
            rect.x = randomNumber.nextInt(200-100)+100;
            rect.y = randomNumber.nextInt(200-100)+100;
            rect.c = new Color(200, 50, 50);

            this.rectangles.add(rect);
        }*/

        for (int i = 0; i < numberOfRect; i++) {
            Rectangle rect = new Rectangle();
            //rectangles only inside the frame we set up
            rect.x = randomNumber.nextInt(540-10)+10;
            rect.y = randomNumber.nextInt(550-60)+60;
            rect.c = new Color(200, 50, 50);

            this.rectangles.add(rect);
        }

    }
    private void removeRectangle() {
        int numberOfRect = rectangles.size();
        Random randomNumber = new Random();

        for (int i = 0; i < numberOfRect/2; i++) {
            //removes half of the rectangles in the list randomly
            rectangles.remove(randomNumber.nextInt(rectangles.size()));
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //it the action performed is this, either run add or remove
        if(e.getSource() == more){
            addRectangle();
        }
        if(e.getSource() == less){
            removeRectangle();
        }
        repaint();
    }
}

public class E20_7 {
    public static void main(String[] args) {

        new RectangleFrame();

    }
}
