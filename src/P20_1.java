import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class RectangleFrame2 extends JFrame implements ChangeListener {

    JSlider slider = new JSlider(0, 50, 0);
    private List<Rectangle> rectangles = new ArrayList<>();



    private class Rectangle{
        public int x;
        public int y;
        public Color c;
    }

    public RectangleFrame2(){
        super("Random Rectangles");
        setSize(600,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        add(slider);

        JPanel panel = new JPanel();
        panel.add(slider);


        add(panel);

        slider.addChangeListener(this);

        setVisible(true);
    }

    public void paint (Graphics g){
        super.paint(g);
        for (Rectangle r: rectangles){
            paint(g,r);
        }
    }

    private void paint(Graphics g, Rectangle r) {
        g.setColor(r.c);
        g.fillRect(r.x,r.y,50,50);
        g.setColor(Color.BLACK);
        g.drawRect(r.x,r.y ,50,50);
    }

    public void addOrRemoveRectangle(){
        int numberOfRect = rectangles.size();
        int sliderValue = slider.getValue();

        Random randomNumber = new Random();

        if(numberOfRect < sliderValue){
            Rectangle rect = new Rectangle();
            //rectangles only inside the frame we set up
            rect.x = randomNumber.nextInt(540-10)+10;
            rect.y = randomNumber.nextInt(550-60)+60;
            rect.c = new Color(200, 50, 50);

            this.rectangles.add(rect);
        }
        if(numberOfRect > sliderValue){
            //removes half of the rectangles in the list randomly
            rectangles.remove(randomNumber.nextInt(rectangles.size()));
        }

    }

    @Override
    public void stateChanged(ChangeEvent e) {

        addOrRemoveRectangle();
        repaint();

    }


}

public class P20_1 {
    public static void main(String[] args) {

        new RectangleFrame2();

    }
}
