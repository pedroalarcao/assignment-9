import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class SavingsAccount extends JFrame implements ActionListener {


    JLabel savingsLabel = new JLabel("Enter amount in Savings account: ");
    JTextField savingsField = new JFormattedTextField();

    JLabel interestRateLabel = new JLabel("Enter the Annual Interest rate: ");
    JTextField interestRateField = new JFormattedTextField();

    JLabel yearsLabel = new JLabel("Enter years: ");
    JTextField yearsField = new JFormattedTextField();

    JTextArea resultArea = new JTextArea(10, 30);

    JScrollPane scrollBar = new JScrollPane(resultArea);

    JButton calculate = new JButton("Calculate");


    public SavingsAccount(){
        super("High Annual Interest rate Saving Account");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        resultArea.setEditable(false);

        savingsField.setMinimumSize(new Dimension(100,20));
        savingsField.setPreferredSize(new Dimension(100,20));
        savingsField.setMaximumSize(new Dimension(100,20));

        interestRateField.setMinimumSize(new Dimension(100,20));
        interestRateField.setPreferredSize(new Dimension(100,20));
        interestRateField.setMaximumSize(new Dimension(100,20));

        JPanel panel = new JPanel();
        GroupLayout layout = new GroupLayout(panel);



        scrollBar.setVerticalScrollBarPolicy (ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);


        panel.setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);


        //notes: the text area was added through the fact that the scroll bar JScroll included the text area within it. Took a lot of trial and error for layout.
        layout.setHorizontalGroup(layout
                        .createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(savingsLabel, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(savingsField, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(interestRateLabel, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(interestRateField, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(yearsLabel, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(yearsField, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(scrollBar))
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(calculate)));

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(savingsLabel).addComponent(savingsField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(interestRateLabel).addComponent(interestRateField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(yearsLabel).addComponent(yearsField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(scrollBar))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(calculate)));


        calculate.addActionListener(this);


        add(panel);
        pack();

        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        double balance = Double.parseDouble(savingsField.getText());
        double rate = Double.parseDouble(interestRateField.getText());
        int years = Integer.parseInt(yearsField.getText());


        for (int i = 0; i < years; i++){
            double interest = balance * rate / 100;

            balance = balance + interest;

            resultArea.append(String.format("%.2f", balance) + "\n");
        }


    }
}

public class P20_3 {
    public static void main(String[] args) {
        new SavingsAccount();
    }
}
